(ns factoidic.durability.server
  (:require [mount.core :refer [defstate]]
            [factoidic.util :as u])
  (:import [org.h2.tools Server]))

(defn- create-server
  ""
  []
  (let [server (Server/createTcpServer
                (into-array String []))]
    server))

(defn- disconnect
  ""
  [server]
  (.stop server))

(defstate server
  :start (create-server)
  :stop (disconnect server))

(defn start "" []
  (u/run-if-started server
                    #(if (.isRunning server false)
                       ::already-running
                       (.start server))))
