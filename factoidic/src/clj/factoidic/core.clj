(ns factoidic.core
  (:require [clojure.spec.alpha :as s]
            [factoidic.config :as conf]
            [factoidic.durability.server :as storage-server]
            [factoidic.db :as db]
            [mount.core :as mount]
            [datascript.core :as ds])
  (:gen-class))


(defn start
  ""
  [config-map]
  (if (s/valid? ::conf/config config-map)
    (assoc (mount/start (mount/with-args config-map))
           :storage (storage-server/start))
    (s/explain-data ::conf/config config-map)))

(defn stop
  ""
  []
  (mount/stop))

(defn -main
  ""
  [& args]
  (start #:factoidic.config
         {:storage
          #:factoidic.config{:db-conn
                             {:classname   "org.h2.Driver"
                              :subprotocol "h2:mem"
                              :subname     "test;DB_CLOSE_DELAY=-1"
                              ;; :subname     "./factoidic;DB_CLOSE_DELAY=-1"
                              :user        "sa"
                              :password    ""}}
          :schema {:aka {:db/cardinality :db.cardinality/many}}}))

;; API

(def query db/query)
(def transact! db/transact!)

(comment
  (transact! [{:db/id -1
               :name  "Maksim"
               :age   45
               :aka   ["Max Otto von Stierlitz", "Jack Ryan"] } ]
             {:a "some metadata"})
  )
