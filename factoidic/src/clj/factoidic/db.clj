(ns factoidic.db
  (:require [factoidic.durability :as d]
            [factoidic.util :as u]
            [datascript.core :as ds]
            [mount.core :refer [defstate]]))

(defn -recover-last
  "recover state from durable storage"
  []
  (let [xf (comp (map :datoms)
              (mapcat #(clojure.edn/read-string {:readers ds/data-readers} %)))
        datoms (into [] xf (d/init-factoids))
        schema (d/init-schema)]
    (ds/conn-from-datoms datoms schema)))

(declare listen-tx!)

(defn -init
  ""
  []
  (let [conn (-recover-last)
        sync (atom @conn)]
    {:conn conn
     :sync sync
     :listener (ds/listen! conn listen-tx!)}
    ))

(defn -shutdown
  ""
  [{:keys [conn listener]}]
  (ds/unlisten! conn listener))

(defstate state
  :start (-init)
  :stop (-shutdown state))

(defn listen-tx!
  ""
  [{:keys [db-before db-after tx-data tempids tx-meta]
    :as report}]
  (let [stored (d/store-tx! (:db/current-tx tempids) tx-data tx-meta)]
    (if stored
      (reset! (:sync state) db-after)
      (reset! (:conn state) db-before)
      )))

(defn transact!
  ""
  ([transaction]
   (transact! transaction nil))
  ([transaction metadata]
   (u/run-if-started state
                     #(ds/transact! (:conn state)
                                    transaction
                                    metadata))))

(defn query
  ""
  [query inputs]
  (u/run-if-started state
                    #(apply ds/q query
                            (cons @(:sync state)
                                  inputs))))
