(ns factoidic.durability
  (:require [mount.core :refer [defstate args]]
            [hugsql.core :refer [def-db-fns]]
            [factoidic.util :as u]
            [factoidic.config :as conf]
            [taoensso.nippy :as nippy]
            [datascript.core :as ds]
            [clojure.java.jdbc :as jdbc])
  (:import [datascript.db Datom]))

(declare init-schema)

(def-db-fns "factoidic/h2.sql" {:quoting :off})

(defn- init-db
  ""
  [conf]
  (let [{{db-settings ::conf/db-conn} ::conf/storage
         schema                       ::conf/schema} conf
        create-tables (juxt create-txs
                            create-factoids
                            create-snapshots
                            create-schemas)]
    (jdbc/with-db-transaction [tx db-settings]
      (create-tables tx)
      (let [last-schema (init-schema db-settings)]
        (when (and schema
                 (not= schema last-schema))
          (insert-schema tx {:nippy (nippy/freeze schema)}))))
    db-settings))

(defstate conn
  :start (init-db (args))
  :stop {})

(defn init-factoids "" []
  (all-factoids conn))

(defn init-schema
  ([] (init-schema conn))
  ([c] (some-> (last-schema c) :nippy nippy/thaw)))

(defn- transact-tx!
  ""
  [ds-id tx metadata]
  (jdbc/with-db-transaction [tx-conn conn]
    (let [tx-result (jdbc/insert! tx-conn
                                :tx
                                {:tx ds-id
                                 :meta (if meta (pr-str metadata) nil)})
        tx-id (-> tx-result first vals first)]
      (jdbc/insert! tx-conn :factoid {:datoms (pr-str tx)
                                      :tx tx-id})
      tx-id)))

(defn store-tx!
  ""
  [ds-id tx metadata]
  (let [tx-result (u/run-if-started conn
                                  #(transact-tx! ds-id tx metadata))]
    (if (= tx-result :state/started)
      false
      tx-result)))
