(ns factoidic.util
  (:require [mount.core :as mount]))

(defn run-if-started
  ""
  [state func]
  (if (isa? (type state) mount.core.NotStartedState)
    :state/not-started
    (func)))
