(ns factoidic.config
  (:require [clojure.spec.alpha :as s]))


(s/def :db/index #{true})
(s/def :db/isComponent #{true})
(s/def :db/valueType #{:db.type/ref})
(s/def :db/cardinality #{:db.cardinality/many})
(s/def ::datascript-schema (s/keys :opt [:db/cardinality
                                         :db/valueType
                                         :db/isComponent
                                         :db/index]))
(s/def ::schema (s/map-of keyword? ::datascript-schema))

(s/def ::classname #{"org.h2.Driver"})
(s/def ::subprotocol #{"h2:mem" "h2:file"})
(s/def ::subname string?)
(s/def ::user string?)
(s/def ::password string?)
(s/def ::db-conn (s/keys :req-un [::classname
                                  ::subprotocol
                                  ::subname]
                         :opt-un [::user
                                  ::password]))
(s/def ::storage (s/keys :req [::db-conn]))

(s/def ::config (s/keys :req [::storage]
                        :opt [::schema]))
