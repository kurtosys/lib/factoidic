-- :name create-txs :!
-- :doc create txs table
create table if not exists TX (
  id integer AUTO_INCREMENT PRIMARY KEY,
  tx integer NOT NULL UNIQUE,
  instant timestamp DEFAULT NOW() NOT NULL,
  meta varchar(2048),
)

-- :name create-factoids :!
-- :doc create factoids table
create table if not exists FACTOID (
  id integer AUTO_INCREMENT PRIMARY KEY,
  datoms varchar(4096),
  tx integer NOT NULL UNIQUE,
  FOREIGN KEY (tx) REFERENCES tx(id)
)

-- :name create-snapshots :!
-- :doc create snapshots table
create table if not exists SNAPSHOTS (
  id integer PRIMARY KEY AUTO_INCREMENT,
  db blob,
  tx integer NOT NULL UNIQUE,
  FOREIGN KEY (tx) REFERENCES tx(id),
)

-- :name create-schemas :!
-- :doc create schemas table
create table if not exists DBSCHEMA (
  id integer PRIMARY KEY AUTO_INCREMENT,
  instant timestamp DEFAULT NOW() NOT NULL,
  nippy binary
)

-- :name insert-schema :! :n
-- :doc insert a new schema
insert into dbschema(nippy)
  values(:nippy)

-- :name last-schema :? :1
-- :doc get last known schema
select top 1 nippy
from dbschema
order by instant desc

-- :name all-schemas :?
select *
from dbschema


-- :name all-factoids :?
-- :doc get all factoids, full history
select *
from factoid

-- :name all-txs :?
-- :doc get all transactions
select *
from tx
