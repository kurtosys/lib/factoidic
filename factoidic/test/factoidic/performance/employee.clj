(ns factoidic.performance.employee
  (:require [clojure.test :refer :all]
            [criterium.core :as crit]
            [clojure.xml :as xml]
            [clojure.java.io :as io]
            [com.rpl.specter :as nav]
            [factoidic.core :as f]
            [mount.core :as mount]))

(comment
  (time (f/transact! (map
                      (fn[e] {:db/id ...
                             :employee/data (:content e)
                             :period/start (get-in e [:attrs :tstart])
                             :period/end (get-in e [:attrs :tend])})
                      t)))

  (f/query '[:find ?e ?d
             :in $
             :where [?e :period/end]
             [?e :employee/data ?d]]
           nil)

  (Integer/parseInt (subs "d001" 1))

  (f/start
   #:factoidic.config
   {:storage
    #:factoidic.config{:db-conn
                       {:classname   "org.h2.Driver"
                        :subprotocol "h2:mem"
                        :subname     "performance-test;DB_CLOSE_DELAY=-1"
                        ;; :subname     "./factoidic;DB_CLOSE_DELAY=-1"
                        :user        "sa"
                        :password    ""}}
    :schema {:empl/dept {:db/cardinality :db.cardinality/many}
             :empl.dept/no {:db/valueType :db.type/ref}
             :empl/salary {:db/cardinality :db.cardinality/many}
             :empl/title {:db/cardinality :db.cardinality/many}
             :dept/mgr {:db/cardinality :db.cardinality/many}
             :dept.mgr/no {:db/valueType :db.type/ref}}})

  )

(def files {:dpts "resources/datasets/employees/departments.xml"})

(def DEPT-NO (nav/comp-paths [:content
                              nav/ALL
                              (nav/pred (fn [m] (= (:tag m) :deptno)))
                              :content
                              nav/FIRST
                              ]))

(defn- dept-no "" [dept]
  (->> dept
       (nav/select-one DEPT-NO)
       (#(subs % 1))
       Integer/parseInt))

(defn- init "" []
  (f/start
   #:factoidic.config
   {:storage
    #:factoidic.config{:db-conn
                       {:classname   "org.h2.Driver"
                        :subprotocol "h2:mem"
                        :subname     "factoidic-test;DB_CLOSE_DELAY=-1"
                        ;; :subname     "./factoidic;DB_CLOSE_DELAY=-1"
                        :user        "sa"
                        :password    ""}}
    :schema {:empl/dept {:db/cardinality :db.cardinality/many}
             :empl.dept/no {:db/valueType :db.type/ref}
             :empl/salary {:db/cardinality :db.cardinality/many}
             :empl/title {:db/cardinality :db.cardinality/many}
             :dept/mgr {:db/cardinality :db.cardinality/many}
             :dept.mgr/no {:db/valueType :db.type/ref}}}))

(defn- destroy "" []
  (f/stop))

(defn db-fixture [f]
  (init)
  (f)
  (destroy))

(use-fixtures :each db-fixture)

(defn import-file
  ""
  [f]
  (-> f io/file xml/parse :content))

(defn transact-depts "" [ds]
  (f/transact! (map
                (fn[e] {:db/id (- (dept-no e))
                       :employee/data (-> e :content first)
                       :period/start (get-in e [:attrs :tstart])
                       :period/end (get-in e [:attrs :tend])})
                ds)))

(deftest transaction
  (testing "transaction"
    (testing "many small ones"
      (let [txs (->> files :dpts import-file (take 2) doall)
            bench (-> txs
                      transact-depts
                      (crit/quick-benchmark {}))
            total-time (map #(->> bench
                                  :execution-count
                                  (/ %)
                                  double)
                            (:samples bench))
            datoms (map #(-> % :tx-data count ) (:results bench))
            res {:tx-count (:execution-count bench) ;; number of txs per run
                 :total-time total-time             ;; total nanoseconds per run
                 :throughput (map (fn[[d t]] (/ d  (* crit/ns-to-s t))) ;; average datoms/s
                                  (map vector datoms total-time))}]
        (println res)
        (is true)
        ))))
