(def project 'factoidic)
(def version "0.1.0-SNAPSHOT")

(set-env! :resource-paths #{"resources" "src/clj" "src/sql"}
          :source-paths   #{"test"}
          :dependencies   '[[org.clojure/clojure "RELEASE"]
                            [org.clojure/java.jdbc "0.7.0"]
                            [com.h2database/h2 "1.4.196"]
                            [com.rpl/specter "1.0.2"]
                            [datascript "0.16.1"]
                            [mount "0.1.11"]
                            [com.layerware/hugsql "0.4.7"]
                            [com.taoensso/nippy "2.13.0"]
                            [org.tinylog/tinylog "1.2"]
                            [adzerk/boot-test "RELEASE" :scope "test"]
                            [tolitius/boot-check "RELEASE" :scope "test"]
                            [criterium "0.4.4" :scope "test"]])

(task-options!
 aot {:namespace   #{'factoidic.core}}
 pom {:project     project
      :version     version
      :description "FIXME: write description"
      :url         "http://example/FIXME"
      :scm         {:url "https://github.com/yourname/factoidic"}
      :license     {"Eclipse Public License"
                    "http://www.eclipse.org/legal/epl-v10.html"}}
 jar {:main        'factoidic.core
      :file        (str "factoidic-" version "-standalone.jar")})

(deftask build
  "Build the project locally as a JAR."
  [d dir PATH #{str} "the set of directories to write to (target)."]
  (let [dir (if (seq dir) dir #{"target"})]
    (comp (aot) (pom) (uber) (jar) (target :dir dir))))

(deftask run
  "Run the project."
  [a args ARG [str] "the arguments for the application."]
  (require '[factoidic.core :as app])
  (apply (resolve 'app/-main) args))

(require '[adzerk.boot-test :refer [test]]
         '[tolitius.boot-check :as check])

(deftask check-yagni
  ""
  []
  (set-env! :source-paths #{"src/clj"})
  (check/with-yagni
    :options {:entry-points ["factoidic.core/-main"
                             "factoidic.core/query"
                             "factoidic.core/transact!"]}))

(deftask check-sources
  ""
  []
  (set-env! :source-paths #{"src/clj"})
  (comp
   (check-yagni)
   (check/with-eastwood)
   (check/with-kibit)
   (check/with-bikeshed)))

(deftask dev
  ""
  []
  (comp (watch)
        (speak)
        (check-sources)
        (test)))
