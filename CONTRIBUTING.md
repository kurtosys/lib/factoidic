So you want to make this project better!? Great, thanks a lot. Many will 
appreciate your effort, not at least the project maintainers and users.

- [What do I need to know to help?](#user-content-what-do-i-need-to-know-to-help)
- [How do I make a contribution?](#user-content-how-do-i-make-a-contribution)
   - [Adding issues](#user-content-adding-issues)
   - [Contribute code](#user-content-contribute-code)
- [Where can I go for help?](#user-content-where-can-i-go-for-help)

# What do I need to know to help?

If you are looking to help to with a code contribution... well:
* it's written in clojure, but it may be ported to cljs one day 
  (that would be an amazing contribution, by the way!). For now, it uses 
  [boot](https://github.com/boot-clj/boot) build tooling. A few tasks are 
  defined:
  * `boot dev` - if you run this, everytime you save a file, `boot test` and
    `boot check-sources` is run. It will give you an auditive signal to
    notify you if your code is ok or not.
  * `boot check-sources` - runs a few checks on your code. You may check
    the result to improve your code.
  * `boot test` - runs tests
* the main libraries and technologies used in this project are 
  * [datascript](https://github.com/tonsky/datascript)
    for in-memory database and datalog query engine
  * [H2](www.h2database.com/) and [HugSQL](https://github.com/layerware/hugsql) 
    as durable storage and management thereof
  
If you don't feel ready to make a code contribution yet, no problem! 
You can also check out the [documentation issues](https://gitlab.com/kurtosys/lib/factoidic/issues?label_name[]=doc)
or the [design issues](https://gitlab.com/kurtosys/lib/factoidic/issues?label_name[]=design)

If you are interested in making a code contribution and would like to learn 
more about the technologies that we use, check out the list below:

* [datascript](https://github.com/tonsky/datascript)
* [H2](www.h2database.com/)
* [HugSQL](https://github.com/layerware/hugsql) 

  
<!--
    Include bulleted list of
    resources (tutorials, videos, books) that new contributors
    can use to learn what they need to know to contribute to your project
-->


# How do I make a contribution?

Never made an open source contribution before? Wondering how contributions work
in our project? Well, there are several ways to do so...
 
## Adding issues 
 
 

## Contribute code 

Here's a quick rundown!

1. Find an issue that you are interested in addressing or a feature that you
   would like to add. If it's not there, add an issue (question, bug,
   optimization, ...). See Adding issues for more info. 
2. Request to be a contributor (request access)
3. Branch the master `git checkout -b issue/<issue>`, e.g.
   `git checkout -b issue/#4`. You may first check if that issue is not yet
   worked on.
4. Make changed for the issue.
   1. On each commit to any branch, CI will run, executing `boot check-sources` 
     and `boot test`. Make sure these pass locally before submitting to the repo.
   2. Commit ans submit your code. As long as your work is not finished, 
   please add WIP to the title to prevent it to being merged withthe master. See:
     * [gitlab flow](https://about.gitlab.com/2014/09/29/gitlab-flow/)
     * [gitlab ci](https://about.gitlab.com/features/gitlab-ci-cd/)
5. If you're done with the issue, do a final commit and push, don't add `[WIP]` 
   to the title, but add `closes <issue>` in your comment (e.g. `closes #4`). 
   What will happen is:
   1. It will create a merge request. One of the maintainers will doe a code
      review and accept it or not;
   2. Once the merge is completed (without any failure), the issue will be 
      closed.
6. Celebrate if you have a succesful merge!
7. Watch your name being added to the code contributors if it's your first
   contribution.

# Where can I go for help?

If you need help, you can 
* ask questions on  [slack](https://clojurians.slack.com/messages/factoidic), 
* file an [issue](https://gitlab.com/kurtosys/lib/factoidic/issues), 
* contact me directly by email,
* try to find me and have a drink with me.

<!--
# What does the Code of Conduct mean for me?

Our Code of Conduct means that you are responsible for treating everyone on the project with respect and courtesy regardless of their identity. If you are the victim of any inappropriate behavior or comments as described in our Code of Conduct, we are here for you and will do the best to ensure that the abuser is reprimanded appropriately, per our code.
-->