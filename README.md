- [Factoidic](#user-content-factoidic)
- [Why](#user-content-why)
- [Compare with...](#user-content-compare-with)
   - [Datomic](#user-content-datomic)
   - [Datascript](#user-content-datascript)
- [Usage](#user-content-usage)
   - [Setup](#user-content-setup)
   - [Configuration](#user-content-configuration)
   - [Defining a schema](#user-content-defining-a-schema)
   - [Transactions and queries](#user-content-transactions-and-queries)
   - [Examples](#user-content-examples)
- [Performance](#user-content-performance)
   - [Employee dataset (synthetic)](#user-content-employee-dataset-synthetic)
- [Copyright & License](#user-content-license)

# Factoidic

An extremely lightweight, probably pretty poor performant, certainly
non-distributed, deliberatly embeddedable database and datalog query engine 
for clojure.

<!-- `[kurtosys.factoidic "0.1.0"]` -->

It's not released yet...

* For contributions, incl. feature requests and bug reports, please check the 
  [contribution guide](https://gitlab.com/kurtosys/lib/factoidic/blob/master/CONTRIBUTING.md)
* For the most important changes, please check the
  [changelog](https://gitlab.com/kurtosys/lib/factoidic/blob/master/CHANGELOG.md)


# Why

Because:

* datalog rocks
* clojure is awesome
* every app needs some kind of database
* lightweight is good
* embedded has its use cases
* not all apps...
  * need distributed databases
  * write 10000 entities/s, nor do they need extreme performance

# Compare with...

## Datomic

[Datomic](http://www.datomic.com/) is wicket-awesome, no questions asked.
However, there are use cases for which Datomic is not a good fit, and these 
include lightweight applications, limited in budget, such as personal hobby-
projects and applications for small non-profit organizations. 

The license of Datomic is really not encouraging:
* the 'free' version is the heavy-weight version, needing a transactor and peer.
* the 'starter' version can be used more lightweight, using the client-lib, but
it provides updates for 1 year only. It's been argued that you can still use
the starter version as long as you don't update, but that's a flawed argument:
not updating is just increasing your technical debt. Or, I consider it extremely
bad practice to do so. This means, the starter version, with it's client lib, is
useful, for 1 year only.

Datomic doesn't fit for small projects. This results in the fact that many 
projects who could benefit from it, can't. This library is _not_ a replacement
for Datomic and it lacks many features thereof. Factoidic can, however, be used
in small projects to get a feel for Datalog/Datomic.

## Datascript

[datascript](https://github.com/tonsky/datascript) is an amazing library that 
provides an in-memory datalog query engine, with a syntax comparable to the 
datalog implementation of Datomic. Originally, it was aimed to run in the
browser (using clojurescript or javascript).

It has been implemented in clojure as well and I've been using it in a few
projects on the server-side. It's not usable as a database in it's pure form,
since it doesn't provide durability. Also, datascript only remembers the last
state, and can't be used, as Datomic, for 'time-travel'.

Factoidic literally uses datascript as main database, adding both durability and 
the possibility to time travel to it. That's, in it's essence, all factoidic
does. It uses datascript and it's datalog query engine, writing every 
transaction to durable storage. Since every transaction is written, one can 
always load an earlier state in memory.


# Usage


## Setup

## Configuration

## Defining a schema

Factoidic complies with the schema's datascript describes. It means it's not
necessary, and not even allowed in the case of factoidic, to use any other
attributes than (see [config spec]
(https://gitlab.com/kurtosys/lib/factoidic/blob/master/factoidic/src/clj/factoidic/config.clj) ) :

* `:db/index true` - index an attribute
* `:db/isComponent` - make it a component. See [Datomic docs](http://blog.datomic.com/2013/06/component-entities.html) for more info
* `:db/valueType :db.type/ref` - make an attribute a reference to another entity
* `:db/cardinality :db.cardinality/many` - allow for multiple values for an attribute

To understand schema's:

* [Datomic schema](http://docs.datomic.com/schema.html)
* [datascript differences from Datomic] 
  (https://github.com/tonsky/datascript#differences-from-datomic)

## Transactions and queries

Since this library uses datascript, and hence, it's datalog engine, it complies
with the queries and transactions that can be done using 
[datascript](https://github.com/tonsky/datascript). Datascript uses the syntax
similar as [Datomic](http://www.datomic.com/). To learn about transactions and
queries:

* [datascript getting started](https://github.com/tonsky/datascript/wiki/Getting-started)
* [datascript tutorial](https://github.com/kristianmandrup/datascript-tutorial)
* [datascript examples](https://github.com/tonsky/datascript#usage-examples-)
* [Datomic query](http://docs.datomic.com/query.html)
* [Datomic transaction](http://docs.datomic.com/transactions.html)


## Examples



# Performance

## Employee dataset (synthetic)

From: http://timecenter.cs.aau.dk/software.htm

Dataset: http://timecenter.cs.aau.dk/Data/employeeTemporalDataSet.zip

Results: (employee dataset performance)[]

Summary: 


# Contributors

* [Kurt Sys](https://gitlab.com/kurt.sys) / [github](https://github.com/qsys) . [linkedIn](https://www.linkedin.com/in/kurtsys/)

# Copyright & License

Copyright © 2017 kurtosys

Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.

See [LICENSE](https://gitlab.com/kurtosys/lib/factoidic/blob/master/LICENSE)